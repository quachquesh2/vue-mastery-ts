import { defineStore } from "pinia";
import utils from "../utils";
import { postData } from "./../utils/modules/httpRequest";

import type { Subtitle, SubtitleDetail, Tutorial, Video } from "../types/tutorial";

export const useTutorialStore = defineStore({
  id: "tutorials",
  state: () => ({
    tutorials: [] as Tutorial[],
    videos: [] as Video[],
    currentVideo: {} as Video,
    subtitle: {} as Subtitle,
    subtitleContent: "" as string,
    subtitleDetail: [] as SubtitleDetail[],
  }),
  getters: {
    subtitleIsEmpty: (state) => Object.keys(state.subtitle).length === 0,
    getSubtitleTime: (state) => (time: number) => {
      const { subtitleDetail } = state;
      const index = subtitleDetail.findIndex((item: SubtitleDetail) => {
        return item.start <= time && item.end >= time;
      });
      return subtitleDetail[index];
    },
    getVideos: (state) => {
      for (let i = 0; i < state.videos.length - 1; i++) {
        let min = i;
        for (let j = i + 1; j < state.videos.length; j++) {
          const video = state.videos[min];
          const videoNext = state.videos[j];
          const regexVideo = video.title.match(/^(\d*) - .*$/i) as string[];
          const regexVideoNext = videoNext.title.match(/^(\d*) - .*$/i) as string[];

          const numVideo = regexVideo?.length > 1 ? parseInt(regexVideo[1]) : 0;
          const numVideoNext = regexVideoNext?.length > 1 ? parseInt(regexVideoNext[1]) : 0;
          if (numVideo > numVideoNext) {
            min = j;
          }
        }
        if (min !== i) {
          const video = state.videos[i];
          const videoNext = state.videos[min];
          state.videos[i] = videoNext;
          state.videos[min] = video;
        }
      }
      return state.videos;
    },
  },
  actions: {
    async syncTutorials() {
      this.tutorials = [];
      await utils.httpRequest.getData("/tutorials").then((data: Tutorial[]) => {
        this.tutorials = data;
      });
    },
    async syncVideos(tutorialId: number) {
      this.videos = [];
      await utils.httpRequest.getData(`/videos?tutorial_id=${tutorialId}`).then((data: Video[]) => {
        this.videos = data;
      });
    },
    async syncSubtitle(videoId: number) {
      this.subtitle = {} as Subtitle;
      await utils.httpRequest.getData(`/subtitles?video_id=${videoId}`).then((data: Subtitle[]) => {
        if (data.length > 0) this.subtitle = data[0];
      });
    },
    async syncSubtitleContent(url: string) {
      this.subtitleContent = "";
      this.subtitleDetail = [];
      await fetch(url)
        .then((res) => {
          return res.text();
        })
        .then((text) => {
          if (text.indexOf("WEBVTT") === 0) {
            this.subtitleContent = text;
            // convert to array
            const lines = text.split("\n");
            for (let i = 0; i < lines.length; i++) {
              const line = lines[i];
              let regGroup: RegExpMatchArray | null = null;
              if (/-->/i.test(line)) {
                regGroup = line.match(/^.*(\d{2}:\d*:\d*.\d*) --> (\d*:\d*:\d*.\d*).*$/i);
              }
              if (regGroup && regGroup?.length > 2) {
                for (let j = 0; j < regGroup.length - 1; j++) {
                  const group = regGroup[j];
                  if (
                    /^\d*:\d*:\d*.\d*$/i.test(group) &&
                    /^\d*:\d*:\d*.\d*$/i.test(regGroup[j + 1])
                  ) {
                    const startTime = utils.time.parseTime(regGroup[1]);
                    const endTime = utils.time.parseTime(regGroup[2]);
                    this.subtitleDetail.push({
                      start: startTime,
                      end: endTime,
                      content: lines[i + 1],
                    });
                    break;
                  }
                }
              }
            }
          }
        });
    },

    async addData() {
      const allVideo: any = import.meta.glob(
        "@/assets/tutorials/Web Accessibility Fundamentals/Video/*.mp4",
        {
          import: "default",
          eager: true,
        }
      );
      const allSub: any = import.meta.glob(
        "@/assets/tutorials/Web Accessibility Fundamentals/Sub/*.vtt",
        {
          as: "url",
        }
      );

      const tutorial: Tutorial = {
        id: 0,
        title: "Web Accessibility Fundamentals",
      };

      await utils.httpRequest.postData("/tutorials", tutorial).then((data: Tutorial) => {
        tutorial.id = data.id;
      });
      console.log(`Tutorial: ${tutorial.id} - ${tutorial.title}`);

      const video: Video = {
        tutorial_id: tutorial.id,
        url: "",
        title: "",
      } as Video;
      for (const vKey in allVideo) {
        video.url = (allVideo[vKey] as string) || "";
        const regexUrl: string[] = video.url.match(/^.*\/(.*).mp4$/i) as string[];
        video.title = regexUrl?.length > 1 ? regexUrl[1] : "";

        await utils.httpRequest.postData("/videos", video).then(async (data: Video) => {
          if (data && data.title && data.id) {
            for (const sKey in allSub) {
              const subPath = await allSub[sKey]();
              const subName = subPath.match(/^.*\/(.*).vtt$/i)[1];
              const regexTitle: string[] = data.title?.match(/^(\d*) - .*$/i) as string[];
              const vNumber = regexTitle?.length > 1 ? regexTitle[1] : "";
              const sNumber = subName.match(/^(\d*) - .*$/i)[1];
              if (vNumber === sNumber) {
                const sub: Subtitle = {
                  video_id: data.id,
                  url: subPath,
                };
                await utils.httpRequest.postData("/subtitles", sub).then((data: Subtitle) => {
                  console.log(sNumber);
                });
                return true;
              }
            }
          }
        });
        // delay 100ms
        await new Promise((resolve) => {
          setTimeout(() => resolve(true), 100);
        });
      }
    },
  },
});
