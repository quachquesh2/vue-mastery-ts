import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/ListTutorial.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      component: HomeView,
    },
    {
      path: "/:id",
      name: "watch",
      component: () => import("@/views/WatchTutorial.vue"),
    },
  ],
});

export default router;
