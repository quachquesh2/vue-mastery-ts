const modules = import.meta.glob("./modules/*.ts", { eager: true });

const newModules: any = {};
for (const key in modules) {
  const module = modules[key];
  const arrName: string[] | null = key.match(/^.*\/(.*).ts$/i);
  if (arrName && arrName[1]) {
    newModules[arrName[1]] = module;
  }
}

// icons
const iconModules = import.meta.glob("@/assets/images/*.svg", { as: "raw", eager: true });

const iNewModules: any = {};
for (const key in iconModules) {
  const module = iconModules[key];
  const arrName: string[] | null = key.match(/^.*\/(.*).svg$/i);
  if (arrName && arrName[1]) {
    const index = module.indexOf("<svg");
    iNewModules[arrName[1]] = module.substring(index);
  }
}

newModules.icons = iNewModules;

export default newModules;
