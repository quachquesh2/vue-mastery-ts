const BASE_URL = import.meta.env.VITE_API_URL;

export const postData = async (url: string, data: any) => {
  const response = await fetch(BASE_URL + url, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data),
  });
  return response.json();
};

export const getData = async (url: string) => {
  const response = await fetch(BASE_URL + url);
  return response.json();
};
