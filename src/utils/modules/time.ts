export const parseTime = (time: string) => {
  const [hour, minute, second] = time.split(":").map((item) => parseFloat(item));
  return hour * 3600 + minute * 60 + second;
};
