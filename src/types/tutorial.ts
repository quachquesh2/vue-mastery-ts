export interface Tutorial {
  id: number;
  title: string;
}

export interface Video {
  id?: number;
  tutorial_id: number;
  url: string;
  title: string;
}

export interface Subtitle {
  id?: number;
  video_id: number;
  url: string;
}

export interface SubtitleDetail {
  start: number;
  end: number;
  content: string;
}
