import { createApp } from "vue";
import { createPinia } from "pinia";

import App from "./App.vue";
import router from "./router";

import utils from "./utils";

import "./assets/base.css";

const app = createApp(App);

declare module "vue" {
  interface ComponentCustomProperties {
    $utils: typeof utils;
  }
}

app.provide("$utils", utils);

app.use(createPinia());
app.use(router);

app.mount("#app");
