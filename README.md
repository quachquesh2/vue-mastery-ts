# **Hướng dẫn**

- [Tải Vue Mastery](https://drive.google.com/drive/folders/16GaNH43PZKio3ZyuA7iGznKur59Jrgga?usp=sharing)
- [Tải Vue School](https://drive.google.com/drive/folders/1FcokhHNNELONhpE__UHVOtA9Vz7prWRd?usp=sharing)
## ***Yêu cầu sử dụng node.js v14.18+**

```bash
git clone https://gitlab.com/quachquesh2/vue-mastery-ts.git
cd vue-mastery-ts
npm install
```

### 1. Cài đặt **json-server**

```bash
npm install -g json-server
```

### 2. Tải video tutorials rồi giải nén vào thư mục

```
./src/assets/tutorials
```

### 3. Chạy lệnh khởi động **_server_**

```bash
npm run json-server
```

### 4. Chạy lệnh khởi động **_project_**

```bash
npm run dev
```

# **_Phím tắt khi xem video_**

#### - **Space** | **P**: `Pause/Play`

#### - **Left** | **Right**: `Tua video`

# **_Cài auto translation_**

### 1. Cài extension **_eJOY English_**

[Chrome Store](https://chrome.google.com/webstore/detail/ejoy-english-learn-with-m/amfojhdiedpdnlijjbhjnhokbnohfdfb)

### 2. Chuột phải extension -> **_Tùy chọn_**

### 3. Chọn tab **_Tích hợp video_** -> **_Trang tích hợp thủ công_** -> Click nút **_Thêm_**

### 4. Điền thông tin để tích hợp

#### - **Tên miền**: `localhost:8080`

#### - **Element Class**: `video-subtitle`

### 5. Sau khi điền đủ thông tin thì bấm nút **_Thêm_**

### 6. Cài đặt font-size cho auto dịch tại cột **Hiển thị phụ đề** trong trang **_Tích hợp thủ công_**

### 7. Truy cập vào trang **_[localhost:8080](http://localhost:8080)_**. Nếu vào trang **_[127.0.0.1:8080](http://127.0.0.1:8080)_** thì setup **Tên miền**: `127.0.0.1:8080`
